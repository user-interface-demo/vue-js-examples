import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/hello",
      name: "hello",
      component: () => import("./components/Hello")
    },
    {
      path: "/button",
      name: "button",
      component: () => import("./components/MyButton")
    },
    {
      path: "/loop",
      name: "loop",
      component: () => import("./components/Loop")
    },
    {
      path: "/multi",
      name: "multi",
      component: () => import("./components/MultiCompo")
    }
  ]
});
