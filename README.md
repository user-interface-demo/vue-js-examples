# Vue.js Examples

Stuff is based on https://www.youtube.com/watch?v=4deVCNJq3qc

and tips are from here: https://vuejs.org/v2/style-guide/#Priority-A-Rules-Essential-Error-Prevention

### Run local

First start spring project for example `spring-boot-data-jpa-mysql`

First run install of npm

```
npm install
```

then start service

```
npm run serve
```
